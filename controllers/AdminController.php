<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/3/18
 * Time: 3:28 PM
 */

class AdminController extends AdminBase
{

    public function actionIndex(){
        self::checkAdmin();

        require_once (ROOT. '/views/admin/index.php');
        return true;
    }

}