<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 10/30/18
 * Time: 9:57 PM
 */

class UserController
{
    public function actionRegister(){

        $name = '';
        $email = '';
        $password = '';
        $result = false;

        if (isset($_POST['submit'])){
            $name = $_POST['name'];
            $email = $_POST['email'];
            $password = $_POST['password'];


            $errors = false;

            if(!User::checkName($name)){
                $errors[] = 'not corect name';
            }

            if(!User::checkEmail($email)){
                $errors[] = 'not corect email';
            }

            if(!User::checkPassword($password)){
                $errors[] = 'not corect password';
            }
        }

        if(User::checkEmailExists($email)){
            $errors[] = 'email exist';
        }

        if($errors == false){
            $result = User::register($name, $email, $password);
        }

        require_once (ROOT. '/views/user/register.php');

        return true;
    }

    public function actionLogin()
    {
        $email = '';
        $password = '';

        if(isset($_POST['submit'])){
            $email = $_POST['email'];
            $password = $_POST['password'];

            $errors = false;

            if(!User::checkEmail($email)){
                $errors[]= 'not corect email';
            }

            if(!User::checkPassword($password)){
                $errors[] = 'not corect password';
            }

            $userId = User::checkUserData($email, $password);

            if($userId == false){
                $errors = 'not valid input';
            } else{

                User::auth($userId);

                header("Location: /cabinet/");
            }
        }
        require_once (ROOT.'/views/user/login.php');
        return true;
    }


    public function actionLogout(){

        session_start();

        unset($_SESSION["user"]);

        header("Location: /");
    }

}