<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 10/29/18
 * Time: 8:42 PM
 */

class ProductController
{

    public function actionView($productId)
    {

        $categories = Category::getCategoriesList();

        $product = Product::getProductById($productId);

        require_once(ROOT . '/views/product/view.php');
        return true;
    }
}