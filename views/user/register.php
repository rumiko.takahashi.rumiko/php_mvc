<?php include ROOT. '/views/layout/header.php';?>

<section>
    <div class="container">
        <div class="row">

            <div class="col-sm-4 col-sm-offset-4 padding-right">

                <?php if($result): ?>
                    <p>Registration</p>
                <?php else: ?>
                    <?php if(isset($errors) && is_array($errors)):?>
                        <ul>
                            <?php foreach ($errors as $error):?>
                                <li> - <?php echo $error; ?></li>
                            <?php endforeach;?>
                        </ul>
                    <?php endif;?>

                <div class="signup-form">
                    <h2>
                        <form action="#" method="post">
                            <input type="text" name="name" placeholder="Name" value="<?php echo $name;?>"/>
                            <input type="email" name="email" placeholder="E-mail" value="<?php echo $email;?>"/>
                            <input type="password" name="password" placeholder="Pass" value="<?php echo $password;?>"/>
                            <input type="submit" name="submit" class="btn btn-default" value="submit"/>
                        </form>
                    </h2>
                </div>

                <br/>
                <br/>
            <?php endif; ?>
            </div>

        </div>
    </div>
</section>


<?php include ROOT. '/views/layout/footer.php';?>