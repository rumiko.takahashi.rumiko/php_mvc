<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 10/25/18
 * Time: 8:18 PM
 */


class News
{
    public static function getNewsItemById($id)
    {
        $id = intval($id[0]);

        if($id){

            $db = Db::getConnection();

            $result = $db->query('SELECT * FROM news WHERE id='.$id.';');

//            $result->setFetchMode(PDO::FETCH_NUM);
            $result->setFetchMode(PDO::FETCH_ASSOC);

            $newsItem = $result->fetch();
            return $newsItem;
        }
    }

    public static function getNewsList(){

        $db = Db::getConnection();

        $newsList = array();

        $result = $db->query('SELECT id, h1 FROM news;');

        $i = 0;
        while($row = $result->fetch()){
            $newsList[$i]['id'] = $row['id'];
            $newsList[$i]['h1'] = $row['h1'];
            $i++;
        }
        return $newsList;

    }
}