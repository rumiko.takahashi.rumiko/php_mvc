<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 10/27/18
 * Time: 5:02 PM
 */

class Db
{
    public static function getConnection(){
        $paramsPath = ROOT. '/config/db_params.php';
        $params = include ($paramsPath);

        $dsn = "mysql:host={$params['host']};dbname={$params['dbname']}";
        try {
            $db = new PDO($dsn, $params['user'], $params['password']);
        } catch (PDOException $db){
            die($db->getMessage());
        }

        $db->exec("set names utf-8");

        return $db;
    }
}