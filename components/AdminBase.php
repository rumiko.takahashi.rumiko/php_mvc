<?php
/**
 * Created by PhpStorm.
 * User: vbudnik
 * Date: 11/3/18
 * Time: 3:37 PM
 */

abstract class AdminBase
{
    public static function checkAdmin(){
        $userId = User::checkLogged();

        $user = User::getUserById($userId);

        if($user['role'] == 'admin'){
            return true;
        }

        die('Access denied');
    }
}